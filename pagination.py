"""
Function Pagination
"""

def pagination(v_current_page, v_max_data_perpage, v_total_all_data):

    """ Inisiasi Variable Banyak Data yang akan ditampilkan dalam 1 halaman """
    limit_row = int(v_max_data_perpage)
    
    """ Setting Posisi Halaman """
    current_page = int(v_current_page)

    """ Setting Jumlah Data dalam Database """
    total_all_data = int(v_total_all_data)

    """ Proses menghitung limit start dan end, digunakan nanti ketika ambil data dari DB """
    offset = limit_row * (int(current_page)-1)

    """ Proses menghitung jumlah halaman """
    mod_page = total_all_data % limit_row
    total_page = int(total_all_data / limit_row) + (1 if mod_page < limit_row and mod_page > 0 else 0)

    """ Proses menghitung range page berdasarkan current page, digunakan untuk animasi di frontend """
    prev_page = current_page - 3 if current_page >= 4 else 0
    next_page = current_page + 2 if current_page <= total_page - 3 else total_page
    range_prev_next_page = range(prev_page, next_page)
    
    result = {
        "limit_row":limit_row,
        "current_page":current_page,
        "total_page":total_page,
        "range_prev_next_page":range_prev_next_page,
        "offset":offset
    }

    return result

if __name__ == '__main__':

    current_page = 1
    max_data_perpage = 20
    total_all_data = 108

    paging = pagination(current_page, max_data_perpage, total_all_data)
    print(paging)