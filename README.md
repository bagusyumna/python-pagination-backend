Database : <b>PostgreSQL & MySQL</b>

Tipe     : Native Query

Example Query :

    select * 
    from <:table_name:>
    limit 20
    offset 0

Example Case :

    Total Data : 108 data 
    Limit Data : 20

    # Proses olah data dari backend menghasilkan data :

        - 20 data per halaman
        - total 6 halaman

    # Implementasi di database sesuai halaman :

        - halaman 1 limit 20 offset 0
        - halaman 2 limit 20 offset 20
        - halaman 3 limit 20 offset 40
        - halaman 4 limit 20 offset 60
        - halaman 5 limit 20 offset 80
        - halaman 6 limit 20 offset 100

reference : https://levelup.gitconnected.com/creating-a-data-pagination-function-in-postgresql-2a032084af54